
package supermercado;

import jade.core.AID;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Enumeration;
import javax.swing.border.EmptyBorder;

public class GuiVendaProductos extends JFrame{
    private Agente_Cajero_Vendador miAgente;
    private JTextField CampoTitulo, CampoPrecio;
    private JPanel contentPane;
    public static JList listProduct;
    //Crear FRAME.
    public GuiVendaProductos(final Agente_Cajero_Vendador agent){
        setTitle("Vendador Productos");
		//Crear Panel
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
			
			e.printStackTrace();
		}
                JLabel lblStore = new JLabel("Lista de productos: ");
		lblStore.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblStore.setBounds(10, 11, 125, 28);
		contentPane.add(lblStore);
		
		listProduct = new JList();
		listProduct.setBounds(10, 50, 125, 200);
		contentPane.add(listProduct);
		
		JLabel lblAddBook = new JLabel("Añadir Producto :");
		lblAddBook.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAddBook.setBounds(210, 11, 125, 45);
		contentPane.add(lblAddBook);
		
		JLabel lblBookname = new JLabel("Nombre :");
		lblBookname.setBounds(150, 102, 72, 14);
		contentPane.add(lblBookname);
		
		CampoTitulo = new JTextField();
		CampoTitulo.setBounds(237, 99, 149, 20);
		contentPane.add(CampoTitulo);
		CampoTitulo.setColumns(10);
		
		JLabel lblPrice = new JLabel("Precio :");
		lblPrice.setBounds(153, 142, 74, 14);
		contentPane.add(lblPrice);
		
		CampoPrecio = new JTextField();
		CampoPrecio.setColumns(10);
		CampoPrecio.setBounds(237, 139, 149, 20);
		contentPane.add(CampoPrecio);
		
		JButton btnAddToStore = new JButton("Añadir: ");
		btnAddToStore.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String key = CampoTitulo.getText();
				int value = Integer.parseInt(CampoPrecio.getText());
				agent.catalogo.put(key, value);
				RefreshList(agent);
			}
		});
		btnAddToStore.setBounds(237, 186, 149, 23);
		contentPane.add(btnAddToStore);
		
	RefreshList(agent);
                
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e){
                agent.doDelete();
                    } 
                });
        
        setResizable(false);
    }
    //Metodo de actualizar lista
    public void RefreshList(Agente_Cajero_Vendador agent){
		listProduct.setModel(GetProduct(agent));
	}
    //Metodo de modelo de lista predeterminado
    public DefaultListModel GetProduct(Agente_Cajero_Vendador agent){
		DefaultListModel m = new DefaultListModel();
		Enumeration<String> books = agent.catalogo.keys();
		while(books.hasMoreElements()){
			String key = books.nextElement();
			m.addElement(key+" ("+agent.catalogo.get(key)+")");
		}
		return m;
	}
}
    
    

