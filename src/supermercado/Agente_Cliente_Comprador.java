/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supermercado;

import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import javax.swing.JOptionPane;

public class Agente_Cliente_Comprador extends Agent{
    
    private String TituloProductoComprar;
    // La lista de agentes vendedores conocidos
    private AID[] AgenteVendadores;
    // Pon las inicializaciones del agente aquí
    @Override
    protected void setup() {
        // Imprimir un mensaje de bienvenida
        System.out.println(" Hola Agente Vendador "+getAID().getName()+" ¿Que producto estas buscando? ");
        // Obtenga el título del producto para comprar como argumento inicial
        Object[] args = getArguments();
        if(args != null && args.length > 0){
            TituloProductoComprar = (String) args[0];
            System.out.println(" Estoy buscando el producto: "+ TituloProductoComprar+"!!!!");
            // Agregue un TickerBehaviour que programe una solicitud a los agentes vendedores
            // cada minuto
            addBehaviour(new TickerBehaviour(this,10000) {
                @Override
                protected void onTick() {
                    System.out.println(" Hola Cajero_Vendador, Quiero comprar "+TituloProductoComprar);
                    // Actualizar la lista de agentes vendedores
                    DFAgentDescription template = new DFAgentDescription();
                    ServiceDescription sd = new ServiceDescription();
                    
                    sd.setType("venda-productos");
                    template.addServices(sd);
                    try {
                        DFAgentDescription[] result = DFService.search(myAgent, template);
                        System.out.println(" Los siguientes agentes vendadores son: ");
                        AgenteVendadores = new AID[result.length];
                        for (int i=0; i<result.length; ++i){
                            AgenteVendadores[i]=result[i].getName();
                            System.out.println(AgenteVendadores[i].getName());
                            
                        }
                    } catch (FIPAException fe) {
                        fe.printStackTrace();
                    }
                    // Realizar la solicitud
                    myAgent.addBehaviour(new PedidoCompra());
                }
            });
        }else{
            // Hacer que el agente termine
            System.out.println(" el producto no esta disponible! ");
            doDelete();
        }        
    }
    // Ponga aquí las operaciones de limpieza del agente
    @Override
    protected void takeDown() {
        // Imprimir un mensaje de despido
        System.out.println(" Agente Cliente_Comprador "+getAID().getName()+" Gracias, vuelva pronto !!!");
    }    
    /**
    * Clase interna RequestPerformer. Este es el comportamiento utilizado por el comprador del producto.
    * agentes para solicitar agentes del vendedor el producto de destino.
    */
    private class PedidoCompra extends Behaviour{
        private AID bestSeller;// El agente que ofrece la mejor oferta.
        private int bestPrice;// El mejor precio ofrecido
        private int repliesCnt =0;// El contador de respuestas de los agentes vendedores
        private MessageTemplate mt;// La plantilla para recibir respuestas
        private int step =0;

        @Override
        public void action() {
            switch(step){
                case 0:
                    // Enviar el cfp a todos los vendedores
                    ACLMessage cfp = new ACLMessage(ACLMessage.CFP);
                    for (int i=0; i<AgenteVendadores.length; ++i){
                        cfp.addReceiver(AgenteVendadores[i]);
                    }
                    cfp.setContent(TituloProductoComprar);
                    cfp.setConversationId("comercial-productos");
                    cfp.setReplyWith("cfp"+System.currentTimeMillis());
                    myAgent.send(cfp);
                    // Prepara la plantilla para obtener propuestas
                    mt = MessageTemplate.and(MessageTemplate.MatchConversationId("comercial-productos"), MessageTemplate.MatchInReplyTo(cfp.getReplyWith()));
                    step=1;
                    break;
                case 1:
                    // Recibe todas las propuestas / rechazos de los agentes vendedores
                    ACLMessage reply = myAgent.receive(mt);
                    if(reply != null){
                        // Respuesta recibida
                        if(reply.getPerformative()==ACLMessage.PROPOSE){
                            // Esta es una oferta
                            int price = Integer.parseInt(reply.getContent());
                            if(bestSeller == null || price < bestPrice){
                                // Esta es la mejor oferta en la actualidad.
                                bestPrice=price;
                                bestSeller=reply.getSender();
                            }
                        }
                        repliesCnt++;
                        if(repliesCnt>=AgenteVendadores.length){
                            // Recibimos todas las respuestas
                            step = 2;
                        }
                    }else{
                        block();
                    }
                    break;
                case 2:
                    // Enviar la orden de compra al vendedor que proporcionó la mejor
                    // oferta
                    ACLMessage order = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
                    order.addReceiver(bestSeller);
                    order.setContent(TituloProductoComprar);
                    order.setConversationId("comercial-productos");
                    order.setReplyWith("Orden"+System.currentTimeMillis());
                    myAgent.send(order);
                    // Prepara la plantilla para obtener la respuesta de la orden de compra
                    mt = MessageTemplate.and(MessageTemplate.MatchConversationId("comercial-productos"), MessageTemplate.MatchInReplyTo(order.getReplyWith()));
                    step =3;
                    break;
                case 3:
                    // Recibe la respuesta de la orden de compra
                    reply = myAgent.receive(mt);
                    if(reply != null){
                        // Respuesta de orden de compra recibida
                        if(reply.getPerformative() == ACLMessage.INFORM){
                            // Compra exitosa. Podemos terminar
                            System.out.println(" Producto "+TituloProductoComprar+" comprado de "+reply.getSender().getName());
                            System.out.println(" Precio: BS "+bestPrice);
                            myAgent.doDelete();
                        }else{
                            System.out.println(" Disculpa, pero el producto ya ha agitado ");
                        }
                        step = 4;
                    }else{
                        block();
                    }
                    break;
            }
        }

        @Override
        public boolean done() {
            if(step == 2 && bestSeller == null){
                System.out.println(" Disculpa, producto "+TituloProductoComprar+" esta agitado ");
            }
            return ((step == 2 && bestSeller == null) || step == 4);
        }
        
    }
}

    
