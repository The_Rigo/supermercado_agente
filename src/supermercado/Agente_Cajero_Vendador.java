/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supermercado;
import jade.core.Agent;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

import java.util.*;

public class Agente_Cajero_Vendador extends Agent{

    Hashtable<String,Integer> catalogo;//
    private GuiVendaProductos myGui;

    @Override
    protected void setup() {
        //Crea el catálogo
        System.out.println("Bienvenido agente vendador "+getAID().getName());
        catalogo = new Hashtable<String, Integer>();
        catalogo.put("carne", Integer.parseInt( String.valueOf( Math.round(Math.random()*12))));
	catalogo.put("arroz", 15);
	catalogo.put("leche", Integer.parseInt( String.valueOf( Math.round(Math.random()*11))));
	catalogo.put("cafe", 10);
	catalogo.put("helado", Integer.parseInt( String.valueOf( Math.round(Math.random()*10))));
        myGui= new GuiVendaProductos(this);
        myGui.setVisible(true);
        // Registre el servicio de venta de productos
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("venda-productos");
        sd.setName("Billetera JADE");
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
        // Agregar el comportamiento que atiende las consultas de los agentes compradores
        addBehaviour(new DemandaOfertaServidor());
        // Agregue el comportamiento al servir órdenes de compra de agentes compradores
        addBehaviour(new OrdenaCompraServidor());        
        
    }
    // Ponga aquí las operaciones de limpieza del agente
    @Override
    protected void takeDown() {
        // Darse de baja
        try {
            DFService.deregister(this);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
        // Cerrar la GUI
        myGui.dispose();
        // Imprimir un mensaje de despido
        System.out.println(" Agente Cajero_vendedor "+getAID().getName()+" TERMINADO...");
    }
    //La GUI lo invoca cuando el usuario agrega un nuevo producto a la venta
    public void ActualizaCatalogo(final String titulo, final int precio){
        addBehaviour(new OneShotBehaviour() {
            @Override
            public void action() {
                catalogo.put(titulo, new Integer(precio));
                System.out.println(" Producto: "+titulo+" esta disponible al precio BS "+precio);
            }
        });
    }
    // Clase interna OfferRequestsServer.
    //Este es el comportamiento utilizado por los agentes vendedores de libros para atender las solicitudes entrantes.
    // para oferta de agentes compradores.
    //Si el libro solicitado está en el catálogo local, el agente vendedor responde
    //con un mensaje PROPOSE que especifica el precio. De lo contrario, un mensaje de REFUSE es devuelto.

   
    private class DemandaOfertaServidor extends CyclicBehaviour{

        @Override
        public void action() {
            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
            ACLMessage msg = myAgent.receive(mt);
            if(msg != null){
                // Mensaje CFP recibido. Procesalo
                String titulo = msg.getContent();
                ACLMessage respuesta = msg.createReply();
                Integer precio = (Integer) catalogo.get(titulo);
                
                if(precio != null){
                    // El producto solicitado está disponible. Responde con el precio
                    respuesta.setPerformative(ACLMessage.PROPOSE);
                    respuesta.setContent(String.valueOf(precio.intValue()));
                }else {
                    // El producto solicitado NO está disponible.
                    respuesta.setPerformative(ACLMessage.REFUSE);
                    respuesta.setContent(" No disponible ");
                }
                myAgent.send(respuesta);
            }else{
                block();
            }
        }
    }
    /**
    Clase interna PurchaseOrdersServer.
    Este es el comportamiento utilizado por los agentes vendedores de libros para servir
    Ofrecer aceptaciones (es decir, órdenes de compra) de los agentes compradores.
    El agente vendedor elimina el producto comprado de su catálogo.
    y responde con un mensaje INFORM para notificar al comprador que el
    La compra se ha completado con éxito.
    */
    private class OrdenaCompraServidor extends CyclicBehaviour{

        @Override
        public void action() {
            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.ACCEPT_PROPOSAL);
            ACLMessage msg = myAgent.receive(mt);
            if (msg != null) {
                String titulo = msg.getContent();
                ACLMessage respuesta = msg.createReply();
                
                Integer precio = (Integer) catalogo.remove(titulo);
                if(precio != null){
                    respuesta.setPerformative(ACLMessage.INFORM);
                    System.out.println(" el producto "+titulo+" fue agitado "+msg.getSender().getName());
                    myGui.RefreshList(Agente_Cajero_Vendador.this);
                }else{
                    respuesta.setPerformative(ACLMessage.FAILURE);
                    respuesta.setContent(" no disponible ");
                }
                myAgent.send(respuesta);
            }else{
                block();
            }
        }
    
    }
}
